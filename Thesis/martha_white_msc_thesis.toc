\contentsline {chapter}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {1.1}Bug Deduplication}{2}
\contentsline {section}{\numberline {1.2}Contributions}{3}
\contentsline {section}{\numberline {1.3}Outline}{5}
\contentsline {chapter}{\numberline {2}Related Work}{6}
\contentsline {section}{\numberline {2.1}Information Retrieval (IR) Techniques}{6}
\contentsline {subsection}{\numberline {2.1.1}IR in Software Engineering}{7}
\contentsline {section}{\numberline {2.2}Bug Report Deduplication}{8}
\contentsline {subsection}{\numberline {2.2.1}Approaches Applying IR Techniques Exclusively}{8}
\contentsline {subsection}{\numberline {2.2.2}Stack Traces based Approaches}{12}
\contentsline {subsection}{\numberline {2.2.3}Textual and Categorical Similarity Based Approaches}{13}
\contentsline {subsection}{\numberline {2.2.4}Topic Model Based Approaches}{14}
\contentsline {section}{\numberline {2.3}Contextual Bug Report Deduplication}{15}
\contentsline {section}{\numberline {2.4}Other Bug Report Related Studies}{16}
\contentsline {chapter}{\numberline {3}The Data Set}{19}
\contentsline {section}{\numberline {3.1}The Lifecycle of Bug Reports}{21}
\contentsline {section}{\numberline {3.2}Software-engineering Context in Bug Descriptions}{23}
\contentsline {chapter}{\numberline {4}Methodology}{26}
\contentsline {section}{\numberline {4.1}Preprocessing}{26}
\contentsline {section}{\numberline {4.2}Textual Similarity Measurement}{28}
\contentsline {section}{\numberline {4.3}Categorical Similarity Measurement}{30}
\contentsline {section}{\numberline {4.4}Contextual Similarity Measurement}{32}
\contentsline {section}{\numberline {4.5}Combining the Measurements}{33}
\contentsline {section}{\numberline {4.6}Prediction}{34}
\contentsline {subsection}{\numberline {4.6.1}Classification}{34}
\contentsline {subsubsection}{\numberline {4.6.1.1}Evaluation Metrics}{37}
\contentsline {subsection}{\numberline {4.6.2}Retrieving the List of the Most Similar Candidates}{37}
\contentsline {subsubsection}{\numberline {4.6.2.1}Cosine Similarity based Metric}{39}
\contentsline {subsubsection}{\numberline {4.6.2.2}Euclidean Distance based Metric}{40}
\contentsline {subsubsection}{\numberline {4.6.2.3}Logistic Regression Based Comparison}{41}
\contentsline {subsubsection}{\numberline {4.6.2.4}Evaluating the List of Candidates}{43}
\contentsline {subsubsection}{\numberline {4.6.2.5}The FastREP algorithm}{46}
\contentsline {chapter}{\numberline {5}Case Studies}{48}
\contentsline {section}{\numberline {5.1}Evaluating the Classification-based Retrieval Method}{48}
\contentsline {subsection}{\numberline {5.1.1}Discussion of Findings}{55}
\contentsline {section}{\numberline {5.2}Effectiveness of Number of Features}{58}
\contentsline {subsection}{\numberline {5.2.1}Discussion of Findings}{58}
\contentsline {section}{\numberline {5.3}Evaluating the List of Candidates}{63}
\contentsline {subsection}{\numberline {5.3.1}Discussion of Findings}{65}
\contentsline {section}{\numberline {5.4}Context Matters}{65}
\contentsline {section}{\numberline {5.5}Threats to Validity}{66}
\contentsline {chapter}{\numberline {6}Conclusions and Future Work}{68}
\contentsline {section}{\numberline {6.1}Contributions}{70}
\contentsline {section}{\numberline {6.2}Future Work}{70}
\contentsline {chapter}{Bibliography}{72}
