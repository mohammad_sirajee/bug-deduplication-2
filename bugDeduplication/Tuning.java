import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.*;



public class Tuning {

	REP rep;
	
	public Tuning(Corpus corps,boolean stemming,boolean reweighting ,String impoWordsFile){

		rep = new REP(corps,stemming,reweighting,impoWordsFile);
	}

	private double sim(Document q, Document doc, Map<String, Double> freeVariables){

		return rep.getREP(doc, q, freeVariables);
	}	

	private double RNC(Document q, Document rel, Document irr, Map<String,Double> freeVariables){

		double Y = sim(irr,q,freeVariables) - sim(rel,q,freeVariables);
		return Math.log10(1+Math.exp(Y));
	}

	private double RNCDerivator(Document q, Document rel, Document irr, Map<String,Double> values, String var, double derivPoint, double dx) {

		values.put(var, derivPoint);
		Double val1 = RNC(q,rel,irr,values);
		values.put(var, derivPoint+dx);
		Double val2 = RNC(q,rel,irr,values);

		return (val2-val1)/dx;
	}
	
	private Map<String,Double> simplifiedParameterTuningAlgorithm(Vector<TSI> TS, int N, 
			double eta, Map<String,Double> freeVariables, String[] nonFixedFreeVariables){
		Collections.shuffle(TS);
		int counter = 0;
		
		for(int n=0;n<N;n++){
			System.out.println("current N: "+ n);
			for(TSI I:TS){
				for(String freeVariable:nonFixedFreeVariables){
					
					double newValue = freeVariables.get(freeVariable)-eta*
							RNCDerivator(I.getQuery(), I.getRel(), I.getIrr(), freeVariables, 
									freeVariable, freeVariables.get(freeVariable), 1e-5);
					freeVariables.put(freeVariable, Math.round(newValue * 10000) / 10000.0);
					
					//System.out.println(RNCDerivator(I.getQuery(), I.getRel(), I.getIrr(), freeVariables, 
					//		freeVariable, freeVariables.get(freeVariable), 1e-5));
				}
			}
		}
		
		return freeVariables;
	}
	
	public Map<String,Double> tuningParametersInREP(Vector<TSI> TS){
		
		Map<String,Double> freeVariables = new HashMap<String, Double>(){{
			put("w1",0.9);
			put("w2",0.2);
			put("w3",2.0);
			put("w4",0.0);
			put("w5",0.7);
			put("w6",0.0);
			put("w7",0.0);
			put("wsumm_uni",3.0);
			put("wdesc_uni",1.0);
			put("bsumm_uni",0.5);
			put("bdesc_uni",1.0);
			put("k1_uni",2.0);
			put("k3_uni",0.0);
			put("wsumm_bi",3.0);
			put("wdesc_bi",1.0);
			put("bsumm_bi",0.5);
			put("bdesc_bi",1.0);
			put("k1_bi",2.0);
			put("k3_bi",0.0);
		}};
		
		String[] nonFixedVariables1 = {"w1","w2","w3","w4","w5","w6","w7","bsumm_uni",
				"bsumm_bi","bdesc_uni","bdesc_bi","wsumm_uni","wsumm_bi","wdesc_uni","wdesc_bi"};
		
		int N=24;
		double eta = 0.001;
		
		System.out.println("before simplefied alghorithm");
		freeVariables = simplifiedParameterTuningAlgorithm(TS, N, eta, freeVariables, nonFixedVariables1);
		System.out.println("after simplefied alghorithm");
		
		String[] nonFixedVariables2 = {"w1","w2","w3","w4","w5","w6","w7","k3_uni","k3_bi"};
		
		freeVariables = simplifiedParameterTuningAlgorithm(TS, N, eta, freeVariables, nonFixedVariables2);
		for(String freeVar: freeVariables.keySet()){
			freeVariables.put(freeVar, Math.abs(freeVariables.get(freeVar)));
		}
		
		return freeVariables;
	}
	
	public void writeToFile(Map<String ,Double> freeVars){

		try{
			// Create file 
			FileWriter fstream = new FileWriter("freeVars.txt");
			BufferedWriter out = new BufferedWriter(fstream);
			for(String freeVariable:freeVars.keySet()){
				out.append(freeVariable+" : "+freeVars.get(freeVariable)+"\n");
			}
			//Close the output stream
			out.close();
		}catch (Exception e){//Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}
	}
	
	public static void main(String[] args) {
		
		Preprocessing prep = new Preprocessing();
		AndroidXmlParser AXP = new AndroidXmlParser();
		/*Integer[] arr = {37520,37427,37198,37018,36995,36893,36870,36791,36747,
				36689,36680,36654,36644,34880,33788,37197,37019,36996,36987,36979,
				28557,26147,25712,25230,24811,23737,23171,22439,22003,21382};
		Vector<Integer> unitTestIds = new Vector<Integer>(Arrays.asList(arr));*/

		Vector<Document> corpus = AXP.getBugs();
		Collections.sort(corpus);
		corpus = prep.process(corpus,false);
		Collections.sort(corpus);
		
		int counter = 0;
		for(Document doc: corpus){
			if(doc.getStatus().equals("Duplicate"))
				counter++;
		}
		System.out.println("number of duplicates: " +counter);
		
		Vector<Document> trainSet = new Vector<Document>(corpus.subList(0, 8200));
		Corpus inputCorpus =  new Corpus(trainSet);
		
		TrainSet TS = new TrainSet(trainSet, 24);
		Tuning tuning = new Tuning(inputCorpus,false,false,"");
		Map<String,Double> freeVariables = tuning.tuningParametersInREP(TS.getTS());
		tuning.writeToFile(freeVariables);
		
	}
	
}
