import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;


public class temp {


	public static void main(String[] args) {

		Set<String> words = new HashSet<String>();
		try{
			// Open the file that is the first 
			// command line parameter
			File folder = new File("NFR");
			
			for(String filename:folder.list()){

				FileInputStream fstream = new FileInputStream("NFR\\"+filename);
				// Get the object of DataInputStream
				DataInputStream in = new DataInputStream(fstream);
				BufferedReader br = new BufferedReader(new InputStreamReader(in));
				String strLine;
				//Read File Line By Line
				while ((strLine = br.readLine()) != null)   {
					// Print the content on the console
					words.add(strLine);
				}
				//Close the input stream
				in.close();
			}
		}catch (Exception e){//Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}
		
		new temp().writeToFile(words);
	}

	public void writeToFile(Set<String> words){

		try{
			// Create file 
			FileWriter fstream = new FileWriter("NFR words.txt");
			BufferedWriter out = new BufferedWriter(fstream);
			for(String word: words){
				out.append(word+"\n");
			}
			//Close the output stream
			out.close();
		}catch (Exception e){//Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}
	}
	
}
