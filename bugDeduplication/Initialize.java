import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;


public class Initialize {

	public Initialize(boolean stemming, boolean reweighting, boolean chronological, boolean tuning ,String importantWordsFile, int trainSetSize){

		Preprocessing prep = new Preprocessing();
		AndroidXmlParser AXP = new AndroidXmlParser();
		Vector<Document> corpus = AXP.getBugs();
		
		Collections.sort(corpus);
		///////////////////angoling
		corpus = new Vector<Document> (corpus.subList(0, 2670));
		
		corpus = prep.process(corpus,stemming);
		Collections.sort(corpus);

		int counter = 0;
		for(Document doc: corpus){
			if(doc.getStatus().equals("Duplicate"))
				counter++;
		}
		System.out.println("number of duplicates: " +counter);

		Vector<Document> trainSet;
		Vector<Document> testSet;
		Map<String,Double> freeVariables = new HashMap<String, Double>(){{
			put("w1",0.9);
			put("w2",0.2);
			put("w3",2.0);
			put("w4",0.0);
			put("w5",0.7);
			put("w6",0.0);
			put("w7",0.0);
			put("wsumm_uni",3.0);
			put("wdesc_uni",1.0);
			put("bsumm_uni",0.5);
			put("bdesc_uni",1.0);
			put("k1_uni",2.0);
			put("k3_uni",0.0);
			put("wsumm_bi",3.0);
			put("wdesc_bi",1.0);
			put("bsumm_bi",0.5);
			put("bdesc_bi",1.0);
			put("k1_bi",2.0);
			put("k3_bi",0.0);
		}};
		
		Corpus inputCorpus; 
		Validate validate;
		Map<Document,Vector<SortableDoc>> duplicate_masters_map;
		if(chronological){
			trainSet = new Vector<Document>(corpus.subList(0, trainSetSize));
			counter = 0;
			for(Document doc: trainSet){
				if(doc.getStatus().equals("Duplicate"))
					counter++;
			}
			System.out.println("number of train duplicates: " +counter);
			testSet = new Vector<Document>(corpus.subList(trainSetSize, corpus.size()));
			inputCorpus =  new Corpus(trainSet);
			
			if(tuning){
				TrainSet TS = new TrainSet(trainSet, 24);
				Tuning tun = new Tuning(inputCorpus,stemming,reweighting,importantWordsFile);
				freeVariables = tun.tuningParametersInREP(TS.getTS());
			}
			
			validate = new Validate(inputCorpus,freeVariables,stemming,reweighting,importantWordsFile,chronological);
			duplicate_masters_map = validate.traverseTestSet(testSet);
		}
		
		else{
			inputCorpus =  new Corpus(corpus);
			validate = new Validate(inputCorpus,freeVariables,stemming,reweighting,importantWordsFile,chronological);
			duplicate_masters_map = validate.get_sorted_duplicate_masters_map();
		}

		validate.writeToFile(reweighting?("with_"+importantWordsFile):"without important words", 0, "map",
				validate.MAP(duplicate_masters_map), (reweighting?("with_"+importantWordsFile+" MAP"):"without important words MAP")+".txt");
		for(int k=1;k<21;k++){
			validate.writeToFile(reweighting?("with_"+importantWordsFile):"without important words", k,"recall"
					,validate.recall(duplicate_masters_map, k),(reweighting?("with_"+importantWordsFile):"without important words")+k+".txt");
		}
	}

	public static void main(String[] args) {
		
		new Initialize(false, true, true, false, "important_words.txt", 1900);
		
	}
	
}
