import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.*;
import java.util.Arrays;
import java.util.*;
import java.util.Map;
import java.util.Vector;
import java.util.regex.Pattern;

import javax.xml.soap.Text;

public class Preprocessing {


	public String removeJunkChars(String input){

		String result = "";
		result = input.replaceAll("Q'E", "'");
		result = result.replaceAll("Q\"E", "\"");
		result = result.replaceAll("Q>E", ">");
		result = result.replaceAll("Q<E", "<");

		return result;
	}

	public Vector<Document> removeAllJunkChars(Vector<Document> input){

		Vector<Document> result = new Vector<Document>();
		for(Document tf: input){

			String newDesc = removeJunkChars(tf.getDescription());
			String newTitle = removeJunkChars(tf.getTitle());
			tf.setDescription(newDesc);
			tf.setTitle(newTitle);
			result.add(tf);

		}

		return result;
	}

	public Vector<Document> removeJunkDocuments(Vector<Document> inputDocuments){

		Map<Integer,Document> idDocMap = new Corpus(inputDocuments).getIdDocMap();
		Vector<Document> result = new Vector<Document>();
		Vector<Integer> junks = new Vector<Integer>();

		for(Document doc: inputDocuments){

			if(doc.getStatus().equals("Duplicate") && !idDocMap.containsKey(doc.getMergeID()))
				junks.add(doc.getBugID());
		}

		for(Document doc:inputDocuments){

			if(junks.contains(doc.getBugID()))
				continue;

			if(doc.getBugID() == 0)
				continue;

			if(doc.getStatus().equals("Duplicate")){

				if(doc.getMergeID() == 0)
					continue;

				Document master = idDocMap.get(doc.getMergeID());

				while(master.getStatus().equals("Duplicate") && master.getMergeID() != 0 && idDocMap.containsKey(master.getMergeID())){
					master = idDocMap.get(master.getMergeID());
				}

				if(master.getStatus().equals("Duplicate"))
					continue;

				else{ 
					if(doc.compareTo(master) < 1)
						continue;

					else
						doc.setMasterID(master.getBugID());
				}
			}
			result.add(doc);
		}
		
		
		return result;
	}

	private HashSet<String> stopWords = null;
	public String StopWordsRemoval(String input){

		String result = "";
		String output = "";
	        
                if ( stopWords == null ) {
		    try {
		    	// Open the file that is the first 
		    	// command line parameter
		    	FileInputStream fstream = new FileInputStream("stop words.txt");
		    	// Get the object of DataInputStream
		    	DataInputStream in = new DataInputStream(fstream);
		    	BufferedReader br = new BufferedReader(new InputStreamReader(in));
		    	String strLine;
                        stopWords = new HashSet<String>();
		    	//Read File Line By Line
		    	while ((strLine = br.readLine()) != null)   {
		    		// Print the content on the console
		    		stopWords.add(strLine);
		    	}
		    	//Close the input stream
		    	in.close();
		    } catch (IOException e) {//Catch exception if any
		    	//System.err.println("Error: " + e.getMessage());
                        throw new Error(e.toString());
		    }
                }
		Pattern p = Pattern.compile(" ");

		String[] inputString = p.split(input);
		Vector<String> outputString = new Vector<String>();

		for(String word: inputString){
			if(!stopWords.contains(word.toLowerCase()))
				outputString.add(word);
		}

		for(String s:outputString)
			output += " "+s;

		p = Pattern.compile("\\W");
		inputString = p.split(output);

		for(String word: inputString){
			if(!stopWords.contains(word.toLowerCase()))
				result += " "+word;
		}

		return result;
	}

	public Vector<Document> allStopWordsRemoval(Vector<Document> inputTextFields){

		Vector<Document> results = new Vector<Document>();

		for(Document tf:inputTextFields){

			String newDescription = StopWordsRemoval(tf.getDescription());
			String newTitle = StopWordsRemoval(tf.getTitle());
			tf.setDescription(newDescription);
			tf.setTitle(newTitle);
			results.add(tf);
		}
		return results;
	}

	public Vector<Document> stemmingAll(Vector<Document> inputTextFields){

		Vector<Document> results = new Vector<Document>();

		for(Document tf:inputTextFields){

			Pattern p = Pattern.compile("[^A-Za-z]");
			String[] splittedDesc = p.split(tf.getDescription());
			String newDescription = "";
			for(String descWord: splittedDesc)
				newDescription += " "+Stemmer.stem(descWord);

			String[] splittedTitle = p.split(tf.getTitle());
			String newTitle = "";
			for(String titleWord: splittedTitle)
				newTitle += " "+Stemmer.stem(titleWord);

			tf.setDescription(newDescription);
			tf.setTitle(newTitle);

			results.add(tf);
		}

		return results;
	}

	public Vector<Document> process(Vector<Document> input, boolean stemming){

		Vector<Document> result = new Vector<Document>();
		input = removeJunkDocuments(input);
		input = removeAllJunkChars(input);
		input = allStopWordsRemoval(input);
		if(stemming){
			input = stemmingAll(input);
			input = allStopWordsRemoval(input);
		}

		Pattern p = Pattern.compile("(\\s)+");
		for(Document tf: input){

			String newDesc = "";
			String newTitle = "";
			String[] description = p.split(tf.getDescription());
			String[] title = p.split(tf.getTitle());

			for(String s: description)
				newDesc += s+" ";

			for(String s: title)
				newTitle += s+" ";

			tf.setTitle(newTitle.toLowerCase());
			tf.setDescription(newDesc.toLowerCase());

			result.add(tf);


		}
		return result;
	}


	public static void main(String[] args) {

		/*Preprocessing prep = new Preprocessing();
		AndroidXmlParser read = new AndroidXmlParser();

		Integer[] arr = {37520,37427,37198,37018,36995,36893,36870,36791,36747,36689,36680,36654,36644,34880,33788,37197,37019,36996,36987,36979};
		Vector<Integer> unitTestIds = new Vector<Integer>(Arrays.asList(arr));
		Vector<Document> textFields = read.getUnitTest(unitTestIds);

		Vector<Document> corpus = prep.process(textFields);
		Vector<Document> preprocessedText = prep.process(textFields);*/

	}

}
