import java.util.*;
import java.util.concurrent.*;

public class REPDumper {
    public static void warn(String warning) {
	System.err.println("WARNING: " + warning );
    }		

    public REPDumper() {
	
    }	
	
    void process(boolean stemming, boolean reweighting, boolean tuning ,String importantWordsFile, int trainSetSize) {
        Preprocessing prep = new Preprocessing();
        AndroidXmlParser AXP = new AndroidXmlParser();
        Vector<Document> corpus = AXP.getBugs();
		
        Collections.sort(corpus);
        ///////////////////angoling
        //corpus = new Vector<Document> (corpus.subList(0, ));
		
        corpus = prep.process(corpus,stemming);
        Collections.sort(corpus);

        int counter = 0;
        for(Document doc: corpus){
            if(doc.getStatus().equals("Duplicate"))
                counter++;
        }
        System.err.println("number of duplicates: " +counter);

        Vector<Document> trainSet;
        Vector<Document> testSet;
        Map<String,Double> freeVariables = new HashMap<String, Double>(){{
                put("w1",0.9);
                put("w2",0.2);
                put("w3",2.0);
                put("w4",0.0);
                put("w5",0.7);
                put("w6",0.0);
                put("w7",0.0);
                put("wsumm_uni",3.0);
                put("wdesc_uni",1.0);
                put("bsumm_uni",0.5);
                put("bdesc_uni",1.0);
                put("k1_uni",2.0);
                put("k3_uni",0.0);
                put("wsumm_bi",3.0);
                put("wdesc_bi",1.0);
                put("bsumm_bi",0.5);
                put("bdesc_bi",1.0);
                put("k1_bi",2.0);
                put("k3_bi",0.0);
            }};
		
        Corpus inputCorpus; 
        Validate validate;
        Map<Document,Vector<SortableDoc>> duplicate_masters_map;

        inputCorpus =  new Corpus(corpus);
        //validate = new Validate(inputCorpus,freeVariables,stemming,reweighting,importantWordsFile,chronological);
        //duplicate_masters_map = validate.get_sorted_duplicate_masters_map();
        //REP rep = validate.rep;
        ExtendedREP rep = new ExtendedREP(inputCorpus , false, false, "");//stemming,reweighting,importantWordsFile);

        String [] dictionaryFiles = {
            "wordlist.efficiency",
            "wordlist.functionality",
            "wordlist.maintainability",
            "wordlist.portability",
            "wordlist.reliability",
            "wordlist.usability"
        };
        Vector< Document > dictionaries = new Vector< Document> ();
        for (String file : dictionaryFiles) {
            System.err.println(file);
            Vector<String> tmp = BM25F.readImportantWordsFile( file, stemming );
            StringBuffer buff = new StringBuffer();
            for (String word: tmp) {
                buff.append(word);
                buff.append(" ");
            }
            String out = buff.toString();
            Document d = new FakeDocument( out );
            dictionaries.add( d );
        }
        //int initialDi = 5803;
        //int initialDi = 33886;
        int initialDi = 37256;
        boolean parallel = true;
        if (parallel) {
            final LinkedBlockingQueue<ArrayList<String>> queue = new LinkedBlockingQueue<ArrayList<String>> ();
            final Vector<Document> fcorpus = corpus;
            final ExtendedREP frep = rep;
            final Map<String,Double> ffreeVariables = freeVariables;
            final Vector<Document> fdictionaries = dictionaries;

            Runtime runtime = Runtime.getRuntime();
        
            int nrOfProcessors = runtime.availableProcessors();
        
            ExecutorService executor = Executors.newFixedThreadPool( nrOfProcessors - 1 );
            int count = 0;
            for (int di = initialDi; di < corpus.size() - 1; di++) {            


                final int fdi = di;

                Runnable r = new Runnable() {
                        public void run() {
                            ArrayList<String> results = new ArrayList<String>();
                            for (int dj = fdi+1; dj < fcorpus.size(); dj++) {
                                String result = extendedREP(fcorpus, frep, fdi, dj, ffreeVariables, fdictionaries);
                                results.add(result);
                            }
                            //queue.add( results );
                            boolean done = false;
                            while (!done) {
                                try {
                                    queue.put( results );
                                    done = true;
                                } catch (InterruptedException e) {
                                    System.err.println( e.toString() );
                                }
			    }
                        }
                    };
                Future throwaway = executor.submit( r );
                count++;
            }
            //blocking take
            // once count is 0 we're done!
            try {
                while(count > 0) {
                    ArrayList<String> res = queue.take();
                    for(String line : res) {
                        System.out.println( line );
                    }
                    count--;
                }
            } catch (InterruptedException e) {
                throw new Error(e.toString());
            }
            executor.shutdown();
        } else {
            for (int di = 0; di < corpus.size() - 1; di++) {            
                for (int dj = di+1; dj < corpus.size(); dj++) {
                    System.out.println( extendedREP(corpus, rep, di, dj, freeVariables, dictionaries) );
                }
            }
        }
    }

    public static String extendedREP(Vector<Document> corpus, ExtendedREP rep, int di, int dj, Map<String,Double> freeVariables, Vector<Document> dictionaries) {
        Document doc1 = corpus.get(di);
        Document doc2 = corpus.get(dj);
        double[] features = rep.getExtendedFeatures(doc1, doc2, freeVariables, dictionaries);
        int id1 = doc1.getBugID();
        int id2 = doc2.getBugID();
        int masterID1 = doc1.getMasterID();
        int masterID2 = doc2.getMasterID();
        boolean isDuplicate = (masterID1 == 0 && masterID2 == 0)?false:((doc1.getMasterID() == masterID2 || masterID1 == id2 || id1 == masterID2));
        StringBuffer line = new StringBuffer("");
        String seperator = ",";
        line.append(id1 + seperator);
        line.append(id2 + seperator);
        for (int i = 0 ; i < features.length; i++) {
            line.append(features[i] + seperator);
        }
        line.append( ((isDuplicate)?1:0));
        return line.toString();
    }


    public static void main(String[] args) {
	REPDumper dumper = new REPDumper();
	//dumper.dumpTo("reps.txt");
        dumper.process( false, // stemming
                        false, // reweighting1
                        false, // tuning
                        "",    // importantwords
                        8200);
    }
}
