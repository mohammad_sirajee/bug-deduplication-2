import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;


public class AndroidXmlParser {

	static final String COMMENT = "comment";
	static final String WHEN = "when";
	static final String WHAT = "what";
	static final String AUTHOR = "author";
	static final String NAME = "name";
	static final String BUGID = "issues:id";
	static final String LABEL = "issues:label";
	static final String STARS = "issues:stars";
	static final String STATE = "issues:state";
	static final String STATUS = "issues:status";
	static final String OPENEDDATE = "published";
	static final String UPDATEDON = "updated";
	static final String CLOSEDON = "issues:closedDate";
	static final String TITLE = "title";
	static final String DESCRIPTION = "content";
	static final String OWNER = "issues:owner";
	static final String USERNAME = "issues:username";
	static final String MERGEDINTO = "issues:mergedInto";


	private List<Bug> readRepo(String repositoryFile) {
		List<Bug> bugs = new ArrayList<Bug>();
		try {
			// First create a new XMLInputFactory
			XMLInputFactory inputFactory = XMLInputFactory.newInstance();
			// Setup a new eventReader
			InputStream in = new FileInputStream(repositoryFile);
			XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
			// Read the XML document\
			Bug bug = null;
			int counter = 0;

			Comment comment = null;
			boolean authorFlag = false;
			boolean ownerFlag = false;
			boolean mergeFlag = false;


			while (eventReader.hasNext()) {
				XMLEvent event = eventReader.nextEvent();


				if (event.isStartElement()) {
					StartElement startElement = event.asStartElement();
					// If we have a item element we create a new item
					if (startElement.getName().getLocalPart().equals(AUTHOR)) {
						bug = new Bug();
						authorFlag = true;
						mergeFlag = false;
						ownerFlag = false;
						// We read the attributes from this tag and add the date
						counter++;
						continue;

					}

					if ((event.asStartElement().getName().getPrefix()+":"+event.asStartElement().getName().getLocalPart())
							.equals(MERGEDINTO)) {
						// We read the attributes from this tag and add the date
						// attribute to our object
						mergeFlag = true;
						continue;
					}

					if (event.isStartElement()) {
						if ((event.asStartElement().getName().getPrefix()+":"+event.asStartElement().getName().getLocalPart())
								.equals(BUGID)) {
							event = eventReader.nextEvent();
							String id = event.isEndElement()?"":event.asCharacters().getData();
							if(mergeFlag){
								bug.setMergeID(id);
								mergeFlag = false;
							}
							else
								bug.setBugid(id);

							continue;
						}
					}

					if (event.isStartElement()) {
						if ((event.asStartElement().getName().getPrefix()+":"+event.asStartElement().getName().getLocalPart())
								.equals(USERNAME)) {
							while(!event.isEndElement()){
								event = eventReader.nextEvent();
								if(ownerFlag){
									bug.setOwner(event.isEndElement()?"":event.asCharacters().getData());
									ownerFlag = false;
								}
							}
							continue;
						}
					}

					if (event.isStartElement()) {
						if (event.asStartElement().getName().getLocalPart()
								.equals(NAME)) {
							while(!event.isEndElement()){
								event = eventReader.nextEvent();
								if(authorFlag){
									bug.setAuthor(event.isEndElement()?"":event.asCharacters().getData());
									authorFlag = false;
								}
							}
							continue;
						}
					}

					if (event.isStartElement()) {
						if (event.asStartElement().getName().getLocalPart()
								.equals(TITLE)) {
							while(!event.isEndElement()){
								event = eventReader.nextEvent();
								bug.setTitle(event.isEndElement()?"":event.asCharacters().getData());
							}
							continue;
						}
					}
					if (event.isStartElement()){
						if ((event.asStartElement().getName().getPrefix()+":"+event.asStartElement().getName().getLocalPart())
								.equals(STATUS)) {
							while(!event.isEndElement()){
								event = eventReader.nextEvent();
								bug.setStatus(event.isEndElement()?"":event.asCharacters().getData());
							}
							continue;
						}
					}

					if (event.isStartElement()){
						if ((event.asStartElement().getName().getPrefix()+":"+event.asStartElement().getName().getLocalPart())
								.equals(STATE)) {
							while(!event.isEndElement()){
								event = eventReader.nextEvent();
								bug.setState(event.isEndElement()?"":event.asCharacters().getData());
							}
							continue;
						}
					}

					if (event.isStartElement()){
						if ((event.asStartElement().getName().getPrefix()+":"+event.asStartElement().getName().getLocalPart())
								.equals(OWNER)) {

							ownerFlag = true;
							continue;
						}
					}
					if (event.isStartElement()){
						if ((event.asStartElement().getName().getPrefix()+":"+event.asStartElement().getName().getLocalPart())
								.equals(CLOSEDON)) {
							while(!event.isEndElement()){
								event = eventReader.nextEvent();
								bug.setClosedOn(event.isEndElement()?"":event.asCharacters().getData());
							}
							continue;
						}
					}
					if (event.isStartElement()){
						if ((event.asStartElement().getName().getPrefix()+":"+event.asStartElement().getName().getLocalPart())
								.equals(LABEL)) {
							String label = "";
							while(!event.isEndElement()){
								event = eventReader.nextEvent();
								label += event.isEndElement()?"":event.asCharacters().getData();
							}
							if(label.startsWith("Type-"))
								bug.setType(label.split("-")[1]);

							if(label.startsWith("Priority-"))
								bug.setPriority(label.split("-")[1]);

							if(label.startsWith("ReportedBy-"))
								bug.setReportedBy(label.split("-")[1]);

							if(label.startsWith("Component-"))
								bug.setComponent(label.split("-")[1]);

							if(label.startsWith("Version-"))
								bug.setVersion(label.split("-")[1]);


							continue;
						}
					}
					if (event.isStartElement()){
						if ((event.asStartElement().getName().getPrefix()+":"+event.asStartElement().getName().getLocalPart())
								.equals(STARS)) {
							while(!event.isEndElement()){
								event = eventReader.nextEvent();
								bug.setStars(event.isEndElement()?"":event.asCharacters().getData());
							}
							continue;
						}
					}
					if (event.isStartElement()){
						if (event.asStartElement().getName().getLocalPart()
								.equals(OPENEDDATE)) {
							while(!event.isEndElement()){
								event = eventReader.nextEvent();
								bug.setOpenedDate(event.isEndElement()?"":event.asCharacters().getData());
							}
							continue;
						}
					}

					if (event.isStartElement()){
						if (event.asStartElement().getName().getLocalPart()
								.equals(DESCRIPTION)) {
							while(!event.isEndElement()){
								event = eventReader.nextEvent();
								bug.setDescription(event.isEndElement()?"":event.asCharacters().getData());
							}
							bugs.add(bug);
							continue;
						}
					}

				}
				// If we reach the end of an item element we add it to the list
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}

		return bugs;
	}


	public Vector<Document> getBugs(){

		Vector<Document> results = new Vector<Document>();
		List<Bug> repo = readRepo("android_issues.xml");


		for (Bug item : repo) {
			/*if((item.getTitle().contains("message")&&item.getStatus().equals("Duplicate"))||
					item.getDescription().contains("message")&&item.getStatus().equals("Duplicate"))
				System.out.println("bug :"+item.getBugid() +" master: "+item.getMergeID());*/

			if(!item.getBugid().equals("")){
				results.add(new Document(item.getDescription(), item.getTitle(),
						Integer.parseInt(item.getBugid()),item.getStatus(),item.getStars(),item.getComponent(),
						item.getType(), item.getPriority(), item.getVersion(), item.getMergeID().equals("")?0:
							Integer.parseInt(item.getMergeID()),item.getOpenedDate(),item.getClosedOn()));
			}

			//Close the output stream
		}

		return results;
	}

	Vector<Document> getUnitTest(Vector<Integer> ids){

		Vector<Document> results = new Vector<Document>();
		List<Bug> repo = readRepo("android_issues.xml");

		for (Bug item : repo) {

			if(item.getBugid()!="" && ids.contains(Integer.parseInt(item.getBugid()))){
				results.add(new Document(item.getDescription(), item.getTitle(), item.getBugid()==""?0:
					Integer.parseInt(item.getBugid()),item.getStatus(),item.getStars(),item.getComponent(),
					item.getType(), item.getPriority(), item.getVersion(), item.getMergeID()==""?0:
						Integer.parseInt(item.getMergeID()),item.getOpenedDate(),item.getClosedOn()));
			}
		}
		return results;
	}

	public Vector<Document> getUnitTest(){

		Vector<Document> results = new Vector<Document>();
		List<Bug> repo = readRepo("android_issues.xml");

		DateFormat  sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'.000Z'");

		Date date = new Date();
		Date earliest = new Date();
		Date latest = new Date();

		for (Bug item : repo) {

			Date openDate;
			try {
				openDate = sdf.parse(item.getOpenedDate());
				String GMTOpenTime = openDate.toGMTString();
				openDate = new Date(GMTOpenTime);

				if(earliest.after(openDate))
					earliest = openDate;
				if(openDate.after(latest))
					latest = openDate;

				if(!item.getClosedOn().equals("")){
					Date closeDate = sdf.parse(item.getClosedOn());
					String GMTCloseTime = closeDate.toGMTString();
					closeDate = new Date(GMTCloseTime);
				}

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println(earliest);
		System.out.println(latest);
		return results;
	}

	public void writeToFile(Vector<Document> documents){

		try{
			// Create file 
			FileWriter fstream = new FileWriter("statuses.txt");
			BufferedWriter out = new BufferedWriter(fstream);
			for(Document doc: documents){
				out.append(doc.getStatus()+"\n");
			}
			//Close the output stream
			out.close();
		}catch (Exception e){//Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}
	}

	public static void main(String[] args) {

		Preprocessing prep = new Preprocessing();
		AndroidXmlParser AXP = new AndroidXmlParser();

		Vector<Document> bugs = prep.process(AXP.getBugs(),false);
		System.out.println(bugs.size());

		Collections.sort(bugs);


		//AXP.writeToFile(bugs);

		Map<Integer,Document> idDocMap = (new Corpus(bugs)).getIdDocMap();

		/*for(Document doc:bugs){

			if(doc.getStatus().equals("Duplicate"))
				System.out.println(idDocMap.get(doc.getMergeID()).getStatus());
		}*/

	}
}
