import java.util.HashMap;
import java.util.Map;
import java.util.Vector;


public class REP {
	
	BM25F bmf_unigram;
	BM25F bmf_bigram;
	Corpus corp;

	public REP(Corpus corps, boolean stemming, boolean reweighting, String importantWordsFile){
		
		corp = corps;
		bmf_unigram = new BM25F(corp, 1, stemming, reweighting, importantWordsFile);
		bmf_bigram = new BM25F(corp, 2,stemming, reweighting, importantWordsFile);
	}
	
	public void update(Corpus corps){
		
		bmf_unigram.update(corps);
		bmf_bigram.update(corps);
		corp = corps;
	}
	
	public double[] getFeatures(Document doc, Document query, Map<String, Double> freeVariables){
		
		double[] features = new double[8];
		features[1] = bmf_unigram.calculateBM25F(doc, query, freeVariables);
		features[2] = bmf_bigram.calculateBM25F(doc, query, freeVariables);
		features[3] = 0;
		features[4] = zero_one_function(doc.getComponent(), query.getComponent());
		features[5] = zero_one_function(doc.getType(), query.getType());
		features[6] = reciprocal_function(corp.getPriorityNumber(doc.getPriority()), 
				corp.getPriorityNumber(query.getPriority()));
		features[7] = reciprocal_function(corp.getVersionNumber(doc.getVersion()), 
				corp.getVersionNumber(query.getVersion()));
		
		return features;
	}
	
	public double getREP(Document doc, Document query, Map<String , Double> freeVariables){
		
		double[] features = getFeatures(doc, query, freeVariables);
		double result = 0;
		
		for(int i=1;i<features.length;i++){
			result += freeVariables.get("w"+i)*features[i];
		}
		return (double)((int)(result*1000))/1000;
	}
	
	private int zero_one_function(Object o1, Object o2){
		
		if(o1.equals(o2))
			return 1;
		
		return 0;
	}

	private double reciprocal_function(double d1, double d2){
		
		return (double)1/(1+Math.abs(d1-d2));
	}
	
	public static void main(String[] args) {
		
		/*REP rep = new REP();
		double ana = 2.5;
		double yasi = 2;
		
		System.out.println(rep.reciprocal_function(ana, yasi));*/
	}
	
}
