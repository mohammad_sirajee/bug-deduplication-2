
public class Comment {

	private String author = "";
	private String when = "";
	private String what = "";
	
	public String toString() {
		return "comment [author=" + author + ", when=" + when + ", what=" + what + "]";
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author += author;
	}

	public String getWhen() {
		return when;
	}

	public void setWhen(String when) {
		this.when += when;
	}

	public String getWhat() {
		return what;
	}

	public void setWhat(String what) {
		this.what += what;
	}
	
			
	
}
