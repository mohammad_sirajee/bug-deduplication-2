import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class BM25F {

	Tfidf tfidf;
	private int n;
	Vector<String> important_words = new Vector<String>();
	private boolean reweighting;

	public BM25F(Corpus corps, int input_n_gram, boolean stemming, boolean reweighting, String importatntWordsFile){

		n = input_n_gram;
		tfidf = new Tfidf(corps,n);
		setReweighting(reweighting);
		if(reweighting)
			important_words = initImportantWords(importatntWordsFile,stemming);
	}
	
	public void update(Corpus corps){
		
		tfidf.updateCorpus(corps);
	}

	public double TFQ(Document doc, String word, double wdesc, double wsumm){

		double TFQ1 = wsumm*countFrequencies(doc.getTitle(), word);
		double TFQ2 = wdesc*countFrequencies(doc.getDescription(), word);

		double TFQ = TFQ1+TFQ2;
		/*if(important_words.contains(word))
			TFQ = TFQ*10;*/
		
		return TFQ;
	}

	public int countFrequencies(String str, String word){

		Pattern pattern = Pattern.compile(word);
		Matcher matcher = pattern.matcher(str);
		int counter = 0;
		while (matcher.find())
			counter++;
		return counter;
	}


	public double WQ(Document doc, String word, double wdesc, double wsumm, double k3){

		double TFQ = TFQ(doc,word,wdesc,wsumm);
		return (double)((k3+1)*TFQ)/(k3+TFQ);
	}

	public double calculateBM25F(Document document, Document query, Map<String,Double> freeVariables){

		Vector<String> documentWords = Util.get_n_grams(document.getDescription(), n);
		documentWords.addAll(Util.get_n_grams(document.getTitle(), n));
		Vector<String> queryWords = Util.get_n_grams(query.getDescription(), n);
		queryWords.addAll(Util.get_n_grams(query.getTitle(), n));
                /*
                return calculateBM25F( documentWords, queryWords, freeVariables );
        }
	public double calculateBM25F(Document document, Vector<String> words, Map<String,Double> freeVariables){

		Vector<String> documentWords = Util.get_n_grams(document.getDescription(), n);
		documentWords.addAll(Util.get_n_grams(document.getTitle(), n));
                return calculateBM25F( documentWords, words, freeVariables );
        
        }
	public double calculateBM25F(Vector<String> documentWords, Vector<String> queryWords, Map<String,Double> freeVariables){
            
                */
		double result = 0;
		Set<String> intersection = new HashSet<String>();


		for(String word:documentWords){

			if( queryWords.contains(word) && !word.equals(""))
				intersection.add(word);
		}

		for(String t:intersection){
			double IDF = tfidf.calculateIDF(t);
			double TFD = tfidf.calculateTfD(document, t, freeVariables.get(n==1?"wsumm_uni":"wsumm_bi") , 
					freeVariables.get(n==1?"wdesc_uni":"wdesc_bi") , freeVariables.get(n==1?"bdesc_uni":"bdesc_bi"), 
					freeVariables.get(n==1?"bsumm_uni":"bsumm_bi"));
			double WQ = WQ(query,t,freeVariables.get(n==1?"wdesc_uni":"wdesc_bi"),
					freeVariables.get(n==1?"wsumm_uni":"wsumm_bi"),freeVariables.get(n==1?"k3_uni":"k3_bi"));

			result += ((this.reweighting && important_words.contains(t))?1000:1)*
					IDF * (TFD/(freeVariables.get(n==1?"k1_uni":"k1_bi")+TFD)) * WQ;
			
		}
		return result;
	}

    public static Vector<String> readImportantWordsFile(String fileName,boolean stemming){

		Vector<String> result = new Vector<String>();
		try{
			// Open the file that is the first 
			// command line parameter
			FileInputStream fstream = new FileInputStream(fileName);
			// Get the object of DataInputStream
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			//Read File Line By Line
			while ((strLine = br.readLine()) != null)   {
				// Print the content on the console
				result.add(stemming?Stemmer.stem(strLine):strLine);
			}
			//Close the input stream
			in.close();
		}catch (Exception e){//Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}
		return result;

        }
	public Vector<String> initImportantWords(String fileName,boolean stemming){
            return readImportantWordsFile( fileName, stemming );
	}

	public static void main(String[] args) {

		//Preprocessing prep = new Preprocessing();
		//AndroidXmlParser read = new AndroidXmlParser();

		//Integer[] arr = {37520,37427,37198,37018,36995,36893,36870,36791,36747,36689,36680,36654,36644,34880,33788,37197,37019,36996,36987,36979};
		//Vector<Integer> unitTestIds = new Vector<Integer>(Arrays.asList(arr));

		//Vector<Document> textFields = read.getBugs();
		//Vector<Document> corpus = new Vector<Document>();

		//Document doc1 = new Document("ana ana from the sina ana", "kan", 1, "new",1);
		//Document doc2 = new Document("sina yasi sina", "k", 2, "new",1);
		//Document doc3 = new Document("a ana from the", "kan", 2, "new",1);

		//corpus.add(doc1);
		//corpus.add(doc2);

		//BM25F bm25f = new BM25F(corpus,1);

		//System.out.println(bm25f.calculateBM25F(doc1, doc3, 0.5));

	}

	public boolean isReweighting() {
		return reweighting;
	}

	public void setReweighting(boolean reweighting) {
		this.reweighting = reweighting;
	}
}
