import java.util.Arrays;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Util {

	public static Vector<String> get_n_grams(String text, int n){

		Vector<String> result = new Vector<String>();
		String patt = "[a-zA-Z]+";
		for(int i=1;i<n;i++)
			patt+= "\\s[a-zA-Z]+";

		Pattern pattern = Pattern.compile(patt); // The regex pattern to use

		Matcher matcher = pattern.matcher(text);   
		int spaceIndex = 0;
		while(matcher.find(spaceIndex)){
			spaceIndex = text.indexOf(" ",spaceIndex)+1;
			result.add(matcher.group());
			if(spaceIndex == 0)
				break;
		}

		return result;
	}
}
